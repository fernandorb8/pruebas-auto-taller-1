module.exports = {
  staticFileGlobs: [
    'index.html',
    'styles/*',
    'scripts/*',
    'images/*'
  ],
  runtimeCaching: [{
    urlPattern: new RegExp('^https://api-ratp.pierre-grimaud.fr/v3/schedules'),
    handler: 'networkFirst'
  }]
};
